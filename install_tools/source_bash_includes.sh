#!/bin/bash
# Desc: source .bas|h_includes folder in .bashrc file

# check for clean flag
if [ "$CLI_ARG" == "clean" ]; then
    echo "* WARNING: cant remove code that sources '.bash_includes/main.sh' from ~/.bashrc !!"
else
    # check if .bashrc exists
    if [ -f ~/.bashrc ] ; then

        # check if .bash_includes already sourced
        if [ "$(cat ~/.bashrc | grep '# source .bash_includes')" == "" ] ; then

            # get user confirmation
            if [ "$CLI_ARG" == "o" ] || [ "$CLI_ARG" == "y" ]; then
                conf="y"
            else
                echo -n "Source .bash_includes in .bashrc? [Y/n]: "
                read conf
            fi;

            if [ "$conf" == "" ] || [ "$conf" == "Y" ] || [ "$conf" == "y" ] ; then

                # log
                echo "* sourcing .bash_includes in .bashrc"

                # add code to .bashrc
                echo "# source .bash_includes" >> ~/.bashrc
                echo "if [ -f ~/.bash_includes/main.sh ]; then" >> ~/.bashrc
                echo "   source ~/.bash_includes/main.sh" >> ~/.bashrc
                echo "fi;" >> ~/.bashrc

            else
                echo "* Skipping ..."
            fi;
        else
            echo "* Skipping ... (sourcing code already exist)"
        fi;
    else
        echo "* Skipping ... (.bashrc not found)"
    fi;
fi;

