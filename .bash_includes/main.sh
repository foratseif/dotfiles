#!/bin/bash

#include the includes in the right order
source ~/.bash_includes/bashrc_ext.sh
source ~/.bash_includes/coloring_lib.sh
source ~/.bash_includes/aliases.sh
source ~/.bash_includes/promt.sh

if [ -d ~/.apps ] ; then
    export PATH=$PATH:~/.apps
    if [ -f ~/.apps/.aliases ] ; then
        source ~/.apps/.aliases
    fi;
fi;

if [ -d ~/Apps ] ; then
    export PATH=$PATH:~/Apps
fi;

if [ -d ~/apps ] ; then
    export PATH=$PATH:~/apps
fi;

# source after cuz of PATH issues
source ~/.bash_includes/python_related.sh
