# Git branch in prompt.
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/‹\1› /'
}

# return bold start tag and color start tag (green or red)
git_color_start(){
    if [ "$(git status 2> /dev/null | grep "clean")" == "" ]; then
        echo -e -n "${s_bold}${s_fg_red}"
    else
        echo -e -n "${s_bold}${s_fg_green}"
    fi;
}

# return bold end tag and color end tag (green or red)
git_color_end(){
    if [ "$(git status 2> /dev/null | grep "nothing to commit, working directory clean")" == "" ]; then
        echo -e -n "${e_bold}${e_fg_red}"
    else
        echo -e -n "${e_bold}${e_fg_green}"
    fi;
}

# parse jobs command to add to promt
parse_jobs(){
    IFS=$'\n';
    JOBS=""
    for i in $(jobs); do
        program=$(basename $(echo $i | awk '{print $3}'))
        temp=$(echo "$i" | awk '{print $1}' | sed -e "s/]/-$program/g" | sed -e "s/\[//g")
        name="[${temp:0: -1}]"

        # add bolding if its the last process set to background
        if [ "${temp: -1}" == "+" ]; then
            if [ "$1" == "nc" ]; then
                name="[${name}]"
            else
                name="${s_bold}${name}${e_bold}"
            fi;
        fi;
        JOBS="${JOBS}${name} "
    done;

    echo -e -n "$JOBS"
}

# check tmux sessions
tmux_sessions(){
    NUM=$(tmux ls 2> /dev/null | wc -l)
    A_NUM=$(tmux ls 2> /dev/null | grep "attached" | wc -l)
    let NUM=$NUM-$A_NUM
    if [ $NUM -gt 0 ] ; then
        echo -e -n "[tmux: $NUM] "
    fi;
}

# get battery percentage
bat_percent(){
    PERC=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage | awk  '{print $2}')
    STATE=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep state | awk  '{print $2}')

    STATE_SYMB=""
    #if [ "$STATE" != "" ] ; then
    #    if [ "$STATE" == "discharging" ]; then
    #        STATE_SYMB=" -"
    #    else
    #        STATE_SYMB=" +"
    #    fi;
    #fi;

    echo -n "$PERC$STATE_SYMB"
}

# parse jobs and show a count of how many programs there are
parse_jobs_simple(){
    if [ "$(jobs)" != "" ]; then
        echo -e -n "[*] "
    fi
}

# -- PS1 CUSTOMIZATION --
if [ "$PROMT_CUSTOMIZATION" != "false" ]; then
    # clear PS1
    export PS1=""

    # prep icons variables
    BATTERY_ICON=""
    PERSON_ICON=""
    FOLDER_ICON=""

    # set icons
    if [ "$PROMT_ICONS" == "true" ] ; then
        BATTERY_ICON="🔋 "
        USER_ICON="👤 "
        FOLDER_ICON="📂 "
    fi

    # set default deliminiter
    if [ "$PROMT_DELIM" == "" ] ; then
        PROMT_DELIM="▶"
    fi

    # set empty delimiter
    if [ "$PROMT_DELIM" == "NONE" ] ; then
        PROMT_DELIM=""
    fi

    # add battery status to start of PS1
    if [ "$PROMT_BATTERY" == "true" ]; then
        # check if color is anabled
        if [ "$PROMT_COLORS" == "false" ]; then
            export PS1=$PS1"\$(bat_percent)"
        else
            export PS1=$PS1"\[$s_fg_black\]\[$s_bg_green\] $BATTERY_ICON\$(bat_percent) \[$e_bg_green\]\[$e_fg_black\]\[$s_bg_red\]\[$s_fg_green\]$PROMT_DELIM\[$e_fg_green\]\[$e_bg_red\]"
        fi;
    fi

    # add user and folder to PS1
    if [ "$PROMT_COLORS" == "false" ]; then
        export PS1=$PS1"\u|\W "
    else
        export PS1=$PS1"\[$s_fg_black\]\[$s_bg_red\] $USER_ICON\u \[$e_bg_red\]\[$e_fg_black\]\[$s_bg_yellow\]\[$s_fg_red\]$PROMT_DELIM\[$e_fg_red\] \[$s_fg_black\]$FOLDER_ICON\W \[$e_fg_black\]\[$e_bg_yellow\]\[$s_fg_yellow\]$PROMT_DELIM\[$e_fg_yellow\] "
    fi;

    # add git branch regonition
    if [ "$PROMT_GIT_BRANCH" != "false" ]; then
        if [ "$PROMT_COLORS" == "false" ] || [ "$PROMT_GIT_BRANCH_DIRTY" == "false" ] ; then
            export PS1=$PS1"\$(parse_git_branch)"
        else
            export PS1=$PS1"\[\$(git_color_start)\]\$(parse_git_branch)\[\$(git_color_end)\]"
        fi;
    fi;

    # add background jobs parsing
    if [ "$PROMT_JOBS" != "false" ]; then
        if [ "$PROMT_COLORS" != "false" ]; then
            if [ "$PROMT_JOBS" == "simple" ]; then
                export PS1=$PS1"\[$s_fg_blue\$(parse_jobs_simple)$e_fg_blue\]"
            else
                export PS1=$PS1"\[$s_fg_blue\$(parse_jobs)$e_fg_blue\]"
            fi
        else
            if [ "$PROMT_JOBS" == "simple" ]; then
                export PS1=$PS1"\$(parse_jobs_simple)"
            else
                export PS1=$PS1"\$(parse_jobs nc)"
            fi
        fi;
    fi;

    # add background tmux sessions check
    if [ "$PROMT_TMUX" != "false" ]; then
        if [ "$PROMT_COLORS" != "false" ]; then
            export PS1=$PS1"\[$s_fg_magenta\]\$(tmux_sessions)\[$e_fg_magenta\]"
        fi;
    fi;

    # only close with dollar no colors are available
    if [ "$PROMT_COLORS" == "false" ]; then
        export PS1=$PS1"$ "
    fi;

    # make bash 2 lines instead of 1
    if [ "$PROMT_NEWLINE" != "false" ]; then
        export PS1=$PS1"\n→ "
    fi;
fi;

