if [ "$(echo $DISPLAY)" == "" ] && [ "$(fgconsole 2> /dev/null)" -eq 1 ] ; then
  exec startx
fi

export TERM=xterm-256color

export MINIKUBE_IN_STYLE=false

