# activate terminal python environment because the other one crashed
if [ "$VIRTUAL_ENV" == "" ] && [ -d "$MY_PYTHON_ENVS_PATH" ] && [ -d "$MY_PYTHON_ENVS_PATH/terminal" ] ; then
    source $MY_PYTHON_ENVS_PATH/__default__/bin/activate
fi

pyvenv(){
    selected_env=""
    delete_env="false"

    if [ "$1" == "--delete" ] ; then
        delete_env="true"
        selected_env=$2
    elif [ "$1" == "--deactivate" ] || [ "$1" == "-d" ] ; then
        if [ "$VIRTUAL_ENV" != "" ] ; then
            deactivate
        fi
        return 0
    else
        selected_env=$1
    fi


    if [ "$selected_env" == "" ] ; then
        if [ "$(which fzf > /dev/null 2>&1 && echo $?)" == "0" ] ; then
            height=$(ls -l $MY_PYTHON_ENVS_PATH | wc -l)
            selected_env=$(ls -l $MY_PYTHON_ENVS_PATH | awk 'NR>1{print $9}' | fzf --print-query --height=$height --prompt="chose selected_env: " --reverse --info=hidden | tail -1)
        elif  [ "$(which dmenu > /dev/null 2>&1 && echo $?)" == "0" ] ; then
            selected_env=$(ls -l $MY_PYTHON_ENVS_PATH | awk 'NR>1{print $9}' | dmenu -l 50)
        else
            echo "Available envs:"
            ls -l $MY_PYTHON_ENVS_PATH
            return 1
        fi
    fi

    if [ "$selected_env" != "" ] ; then
        if [ "$delete_env" == "true" ] ; then
            if [ -d $MY_PYTHON_ENVS_PATH/$selected_env ] ; then
                if [ "$VIRTUAL_ENV" == "$MY_PYTHON_ENVS_PATH/$selected_env" ] ; then
                    deactivate
                fi
                rm -rf $MY_PYTHON_ENVS_PATH/$selected_env/
            else
                echo "Could not find '$selected_env'. Cant delete."
                return 1
            fi
        else
            if [ -d $MY_PYTHON_ENVS_PATH/$selected_env ] ; then
                source $MY_PYTHON_ENVS_PATH/$selected_env/bin/activate
            else
                echo "python env '$selected_env' not found in $MY_PYTHON_ENVS_PATH"

                type yes_or_no &> /dev/null &&
                    yes_or_no "Create it?" &&
                    python3 -m venv "$MY_PYTHON_ENVS_PATH/$selected_env" &&
                    source $MY_PYTHON_ENVS_PATH/$selected_env/bin/activate &&
                    pip install --upgrade pip
            fi
        fi
    fi
}
