alias xx="exit"
alias fucking="sudo"
alias gg="google-chrome"
alias clock="clear; while(true); do echo -n $(date); sleep 1; clear; done"
#alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias shitlab='xr matlab'
alias vim-session='vim -S .session.vim'
alias gitpushall='git remote | xargs -L1 git push --all'
alias vimgit='vim $(git ls-files -o -m --exclude-standard)'
#alias bæsj='bash'

yes_or_no() {
    while true; do
        read -n 1 -p "$@ [Y/n]: " yn
        [ -z "$yn" ] || echo ""
        case $yn in
            [Yy]*) return 0  ;;
            "") return 0 ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}

vim4chan(){
    vim -c "FourVim $1"
}

vimgrep(){
    vim $(grep -rl $1 $2)
}

pdfonline(){
    rm -f /tmp/pdfonline.pdf
    wget $1 -O /tmp/pdfonline.pdf

    if [ "$2" == "" ] ; then
        mimeo /tmp/pdfonline.pdf
    else
        $2 /tmp/pdfonline.pdf
    fi

    rm /tmp/pdfonline.pdf
}

backup(){
    cp -r $1 $1.backup
}

unbackup(){
    mv $1.backup ${1%.backup}
}

ix.io() {
    local opts
    local OPTIND
    [ -f "$HOME/.netrc" ] && opts='-n'
    while getopts ":hd:i:n:" x; do
        case $x in
            h) echo "ix [-d ID] [-i ID] [-n N] [opts]"; return;;
            d) $echo curl $opts -X DELETE ix.io/$OPTARG; return;;
            i) opts="$opts -X PUT"; local id="$OPTARG";;
            n) opts="$opts -F read:1=$OPTARG";;
        esac
    done
    shift $(($OPTIND - 1))
    [ -t 0 ] && {
        local filename="$1"
            shift
            [ "$filename" ] && {
                curl $opts -F f:1=@"$filename" $* ix.io/$id
                            return
                        }
                    echo "^C to cancel, ^D to send."
                }
            curl $opts -F f:1='<-' $* ix.io/$id
        }

xr(){
    $@ &
    disown
    exit
}

gx(){
    gg $@&xx
}

cx(){
    echo "Lauching Chromium ..."
    chromium $@&xx
}

tmpcd(){
    export TMPCD_PREV_DIR=$(pwd)
	pwd > ~/.TMPCD_PREV_DIR
    if [ "$1" != "" ]; then
        cd $1
    fi;
}

cdd(){
    if [ "$TMPCD_PREV_DIR" != "" ]; then
        cd "$TMPCD_PREV_DIR"
    fi;
}

cdf(){
    if [ -f ~/.TMPCD_PREV_DIR ]; then
		cd "$(cat ~/.TMPCD_PREV_DIR)"
    fi;
}

clone_st(){
    cols=$(tput cols)
    rows=$(tput lines)
    st -g "${cols}x${rows}" &
    pid=$!
    disown $pid
}

mv_swap()
{
    local TMPFILE=tmp.$$
    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

k8s_pod(){
    kubectl get pods $@ | awk 'NR>1{print}' | dmenu -l 20 | awk '{print $1}'
}

k8s_bash(){
    kubectl exec -it $(k8s_pod $@) -- bash
}

k8s_add_config_file(){
    # Make a copy of your existing config
    cp ~/.kube/config ~/.kube/config.bak

    # Merge the two config files together into a new config file
    KUBECONFIG=~/.kube/config:$1 kubectl config view --flatten > /tmp/kubectl_config

    # Replace your old config with the new merged config
    mv /tmp/kubectl_config ~/.kube/config

    # (optional) Delete the backup once you confirm everything worked ok
    rm ~/.kube/config.bak
}

k8s_config(){
    case "$1" in
    "add")
        case "$2" in
        "" | "--help")
            echo "provide config file path"
        ;;
        *)
            k8s_add_config_file "$2"
        ;;
        esac
    ;;
    "remove")
        case "$2" in
        "--dmenu")
            choice="$(kubectl config get-contexts | awk 'NR>1{print}' | dmenu -l 10 | awk -F " *" '{print $2}')"
        ;;
        "" | "--help")
            echo "either write config name or --dmenu to chose"
            choice=""
        ;;
        *)
            choice="$2"
        ;;
        esac

        if [ "$choice" != "" ] ; then
            kubectl config delete-context "$choice"
        fi
    ;;
    "use")
        case "$2" in
        "--dmenu")
            choice="$(kubectl config get-contexts | awk 'NR>1{print}' | dmenu -l 10 | awk -F " *" '{print $2}')"
        ;;
        "" | "--help")
            echo "either write config name or --dmenu to chose"
            choice=""
        ;;
        *)
            choice="$2"
        ;;
        esac

        if [ "$choice" != "" ] ; then
            kubectl config use-context "$choice"
        fi

    ;;
    "" | "--help")
        echo "ADD"
        echo "   k8s_config add <path_to_config_yaml>"
        echo "REMOVE"
        echo "   k8s_config remove --dmenu"
        echo "   k8s_config remove <name_of_context_to_remove>"
        echo "USE"
        echo "   k8s_config use --dmenu"
        echo "   k8s_config use <name_of_context_to_use>"
    ;;
    esac
}
