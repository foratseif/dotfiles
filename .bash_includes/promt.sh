# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo "${BRANCH}${STAT} "
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="&${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo "[${bits}]"
	else
		echo ""
	fi
}

parse_jobs(){
	if [ "$(jobs)" != "" ] ; then
		echo "* "
    else
		echo ""
	fi;
}

python_venv(){
	if [ "$VIRTUAL_ENV" != "" ] ; then
        env="$(basename $VIRTUAL_ENV)"

        if [ "$env" == "__default__" ]; then
            # do nothing
            echo ""
	    elif [ "$env" == ".env" ] ||
           [ "$env" == ".venv" ] ;
        then
            echo "($(basename $(dirname $VIRTUAL_ENV))) "
        else
            echo "($env) "
        fi;

    else
		echo ""
	fi;
}

PS1=''
PS1=$PS1'\[\033[35m\]\[\033[1m\][ \w ]\[\033[0m\]\[\033[39m\] '
PS1=$PS1'\[\033[36m\]\[\033[1m\]$(parse_git_branch)\[\033[0m\]\[\033[39m\]'
export VIRTUAL_ENV_DISABLE_PROMPT=1 # disable the default virtualenv prompt change
PS1=$PS1'\[\033[36m\]\[\033[93m\]$(python_venv)\[\033[0m\]\[\033[39m\]'
PS1=$PS1'\[\033[33m\]$(parse_jobs)\[\033[39m\]'
