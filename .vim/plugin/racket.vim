function s:RunInRacket() range
    execute a:firstline.",".a:lastline."w! /tmp/vim_racket.scm"
    execute "!clear && racket -I r5rs -e \"$(cat /tmp/vim_racket.scm)\""
    call delete("/tmp/vim_racket.scm")
endfunction

command -range=% Racket <line1>,<line2>call <SID>RunInRacket()
