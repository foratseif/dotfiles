;; TODO:
;; - check out https://github.com/rougier/dotemacs/blob/master/dotemacs.org
;; - check out https://inflambda.tech/post/2020-11-12-spacemacs-keybinding.html
;; - check out https://gist.github.com/rougier/b15fb6e98fadb6580958b1733659027b
;; - check out https://github.com/noctuid/evil-guide
;; - check out https://github.com/ryanoasis/nerd-fonts
;;
;; ( ) use learn org-mode basics
;; ( ) make .emacs into emacs.org
;; ( ) setup exwm for pinebook

;; make emacs add custom code to custom.el
(setq custom-file (concat user-emacs-directory "/custom.el"))

;; -- CLEAN UP GUI --
(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

(menu-bar-mode -1)          ; Disable the menu bar

;; -- maximize emacs window size
(add-hook 'window-setup-hook 'toggle-frame-maximized t)

;; Custom setq's
(setq vc-follow-symlinks t) ; follow symlinks without asking
(setq visible-bell t) ; Set up the visible bell
(setq-default truncate-lines t) ; remove line wrap
(setq scroll-conservatively 101) ; dont auto center text vertically when scrolling
(setq select-enable-clipboard t)

;; (setq x-select-enable-clipboard nil)
;; (setq gui-select-enable-clipboard t)
;; (setq select-enable-clipboard t)
;; (setq save-interprogram-paste-before-kill t)
;; (setq yank-pop-change-selection t)


;; remove trailing whitespaces
(add-hook 'before-save-hook
          'delete-trailing-whitespace)

;; -- FONT AND THEME
;; DOeSNT WORK -> (set-face-attribute 'default nil :font "Fira Code Retina" :height 280)
(load-theme 'wombat)

;; Make ESC quit prompts
; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; fix kill buffer
(global-set-key (kbd "C-x C-k") 'kill-buffer-and-window)

;; adds all autosave-files (i.e #test.txt#, test.txt~) in one
;; directory, avoid clutter in filesystem.
(defvar emacs-autosave-directory (concat user-emacs-directory "autosaves/"))
(setq backup-directory-alist
      `((".*" . ,emacs-autosave-directory))
      auto-save-file-name-transforms
      `((".*" ,emacs-autosave-directory t)))

;; -- PACAKGE SETUP --
;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; -- LINE NUMBERS --
(column-number-mode)
(global-display-line-numbers-mode t)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
		vterm-mode-hook
                shell-mode-hook
		eshell-mode-hook
		undo-tree-visualizer-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))


;; -- PACKAGES --
(use-package command-log-mode) ; logs commands (with shortcuts)


;; fuzzy find (sorting i think?)
(use-package flx :ensure t)

;; completion framework
(use-package ivy
  :diminish
  :after (flx) ; fuzzy find (fuzzy find works without this, its only for sorting)
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :init
  (ivy-mode 1)
  (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy))))

;; NOTE: the first time you load your config on a new machine, you'll
;; need to run the following command interactively so that mode line icons
;; display correctly:
;;
;; M-x all-the-icons-install-fonts
(use-package all-the-icons)

;; mode line from doom emacs
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; themes from doom emacs
(use-package doom-themes
  ;:init (load-theme 'doom-vibrant t))
  :init (load-theme 'doom-badger t))

;; make delimiter colors colors different to match each other
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; pops up panel to show key completion
(use-package which-key
  :init (which-key-mode 1)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

;; transforms the ivy menu to show command description
(use-package ivy-rich
  :ensure t
  :after (ivy)
  :init
  (setq ivy-rich-path-style 'abbrev
        ivy-virtual-abbreviate 'full)
  :config (ivy-rich-mode 1))

;; installs counsel and uses counsel version of stuff instead of emacs defaults
(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-b" . counsel-switch-buffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

;; alternate to built-in emacs help that provides more contextual information
(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))


;; better for defining keys
(use-package general
  :config

  (general-create-definer mymacs/leader-keys ; set space and control+space as key prefix
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC"))

;;  (mymacs/leader-keys
;;   "t" '(:ignore t :which-key "toggles")
;;   "tt" '(counsel-load-theme :which-key "choose theme"))

(use-package undo-tree
  :config (global-undo-tree-mode 1))
(use-package undo-tree
  :ensure t
  :init
  (setq undo-limit 78643200)
  (setq undo-outer-limit 104857600)
  (setq undo-strong-limit 157286400)
  (setq undo-tree-mode-lighter " UN")
  (setq undo-tree-auto-save-history t)
  (setq undo-tree-enable-undo-in-region nil)
  (setq undo-tree-history-directory-alist '(("." . "~/emacs.d/undo")))

  :config
  (global-undo-tree-mode 1))

;;(use-package evil
;;  :after (undo-tree)
;;  :init
;;  (setq evil-want-integration t)
;;  (setq evil-want-keybinding nil)
;;  (setq evil-want-C-u-scroll t)
;;  (setq evil-want-C-i-jump nil)
;;  (setq evil-undo-system 'undo-tree)
;;  (setq evil-vsplit-window-right t)
;;  (setq evil-split-window-below t)
;;
;;  ;:hook (evil-mode . mymacs/evil-hook)
;;  :config
;;  (evil-mode 1)
;;
;;  ;; C-g jumps to normal mode
;;  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
;;
;;  ;; go into window resize mode
;;  (define-key evil-normal-state-map (kbd "C-w x") 'hydra-evil-window-resize/body)
;;
;;  ;; stop c-h in insert mode
;;  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backwards-char-and-join)
;;
;;  ;; paragraph skip
;;  (define-key evil-normal-state-map (kbd "C-j") 'evil-forward-paragraph)
;;  (define-key evil-normal-state-map (kbd "C-k") 'evil-backward-paragraph)
;;
;;  ;; hook for undo in scrach file
;;  (add-hook 'evil-local-mode-hook 'turn-on-undo-tree-mode))
;;
;;(use-package evil-collection
;;  :after (evil magit)
;;  :config
;;  (evil-collection-init))

(defun meow-setup ()
  (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
  (meow-motion-overwrite-define-key
   '("j" . meow-next)
   '("k" . meow-prev)
   '("<escape>" . ignore))
  (meow-leader-define-key
   ;; SPC p goes to projectile prefix
   '("p" . projectile-command-map)
   ;; SPC j/k will run the original command in MOTION state.
   '("j" . "H-j")
   '("k" . "H-k")
   ;; Use SPC (0-9) for digit arguments.
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   ;; paragraph skip
   '("C-j" . forward-paragraph)
   '("C-k" . backward-paragraph)
   ;; ----
   '("0" . meow-expand-0)
   '("9" . meow-expand-9)
   '("8" . meow-expand-8)
   '("7" . meow-expand-7)
   '("6" . meow-expand-6)
   '("5" . meow-expand-5)
   '("4" . meow-expand-4)
   '("3" . meow-expand-3)
   '("2" . meow-expand-2)
   '("1" . meow-expand-1)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("ø" . meow-beginning-of-thing)
   '("æ" . meow-end-of-thing)
   '("a" . meow-append)
   '("A" . meow-open-below)
   '("b" . meow-back-word)
   '("B" . meow-back-symbol)
   '("c" . meow-change)
   '("d" . meow-delete)
   '("D" . meow-backward-delete)
   '("e" . meow-next-word)
   '("E" . meow-next-symbol)
   '("f" . meow-find)
   '("g" . meow-cancel-selection)
   '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-insert)
   '("I" . meow-open-above)
   '("j" . meow-next)
   '("J" . meow-next-expand)
   '("k" . meow-prev)
   '("K" . meow-prev-expand)
   '("l" . meow-right)
   '("L" . meow-right-expand)
   '("m" . meow-join)
   '("n" . meow-search)
   '("o" . meow-block)
   '("O" . meow-to-block)
   '("p" . meow-yank)
   '("q" . meow-quit)
   '("Q" . meow-goto-line)
   '("r" . meow-replace)
   '("R" . meow-swap-grab)
   '("s" . meow-kill)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . meow-undo-in-selection)
   '("v" . meow-visit)
   '("w" . meow-mark-word)
   '("W" . meow-mark-symbol)
   '("x" . meow-line)
   '("X" . meow-goto-line)
   '("y" . meow-save)
   '("Y" . meow-sync-grab)
   '("z" . meow-pop-selection)
   '("'" . repeat)
   '("<escape>" . ignore)))

(use-package meow
  :after (projectile)
  :config
  (setq meow-use-clipboard t)
  (meow-setup)
  :init
  (meow-global-mode 1))

;; hydra
(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

;; hydra window resize
(defun mymacs/evil-window-line-up (step)
  (if (window-in-direction 'below)
      (evil-window-decrease-height step)
    (evil-window-increase-height step)))

(defun mymacs/evil-window-line-down (step)
  (if (window-in-direction 'below)
      (evil-window-increase-height step)
    (evil-window-decrease-height step)))

(defun mymacs/evil-window-line-left (step)
  (if (window-in-direction 'right)
      (evil-window-decrease-width step)
    (evil-window-increase-width step)))

(defun mymacs/evil-window-line-right (step)
  (if (window-in-direction 'right)
      (evil-window-increase-width step)
    (evil-window-decrease-width step)))

(defhydra hydra-evil-window-resize (:timeout 4)
  ("j" (mymacs/evil-window-line-down 1) "down by 1")
  ("k" (mymacs/evil-window-line-up 1) "up by 1")
  ("l" (mymacs/evil-window-line-right 1) "right by 1")
  ("h" (mymacs/evil-window-line-left 1) "left by 1")
  ("C-j" (mymacs/evil-window-line-down 3) "down by 3")
  ("C-k" (mymacs/evil-window-line-up 3) "up by 3")
  ("C-l" (mymacs/evil-window-line-right 5) "right by 3")
  ("C-h" (mymacs/evil-window-line-left 5) "left by 3")
  ("f" nil "finished" :exit t))

(mymacs/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale-text")
  "ws" '(hydra-evil-window-resize/body :which-key "evil window resize"))

;; -- PROJECTILE --
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  ;:bind-keymap
  ;("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/repos")
    (setq projectile-project-search-path '("~/repos")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

;; -- MAGIT --
(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; -- GIT FRINGE LINE --
(use-package git-gutter
  :hook (prog-mode . git-gutter-mode)
  :config
  (setq git-gutter:update-interval 0.02))


(if (display-graphic-p)
  (use-package git-gutter-fringe
    :config
    (setq-default left-fringe-width  2)
    (setq-default right-fringe-width 0)
    (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:deleted [240] nil nil '(bottom repeated))))

;; vterm for terminal emulator
(use-package vterm :ensure t)


;; LSP mode
(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t))

;; languages
;(use-package tree-sitter
;  :init
;  (global-tree-sitter-mode))
;(use-package tree-sitter-langs
;  :after (tree-sitter))

;; - typescript
(use-package typescript-mode
  :mode "\\.ts'"
  ;:hook (typescript-mode . lsp-deferred)
  :config
  (setq typescript-indent-level 4))

;; - kotlin
(use-package kotlin-mode
  :mode "\\.kt'"
  ;:hook (kotlin-mode . lsp-deferred)
  :config
  (setq kotlin-indent-level 4))

;; - swift
(use-package swift-mode
  :mode "\\.swift'"
  ;:hook (kotlin-mode . lsp-deferred)
  :config
  (setq swift-indent-level 4))

;;; -- LINE NUMBERING --
;(setq-default display-line-numbers 'visual
;              display-line-numbers-widen t
;              ;; this is the default
;              display-line-numbers-current-absolute t)
;
;(defun noct-relative ()
;  "Show relative line numbers."
;  (setq-local display-line-numbers 'visual))
;
;(defun noct-absolute ()
;  "Show absolute line numbers."
;  (setq-local display-line-numbers t))
;
;(add-hook 'evil-insert-state-entry-hook #'noct-absolute)
;(add-hook 'evil-insert-state-exit-hook #'noct-relative)
