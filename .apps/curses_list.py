import curses
import time

STANDOUT=0
DIM_OTHERS=1

_stdscr=None
_choices=[]
_choice_index=0
_highlight=DIM_OTHERS
_cursor=False
_title=''

def    create_list(l):
    if len(l) > 0:
        global _choices
        _choices=l
    else:
        raise AttributeError("List cant be empty")

def select(title='', q_disable=False, highlight=DIM_OTHERS, cursor=False):
    global _stdscr
    _stdscr = curses.initscr()

    global _highlight
    _highlight = highlight

    global _cursor
    _cursor = cursor

    global _title
    _title = title

    curses.noecho()
    _display_list()
    try:
        _keyboard_listen(q_disable)
    except KeyboardInterrupt as e:
        end()
        raise KeyboardInterrupt()
    end()

def get_choice():
    return _choice_index

def end():
    if _stdscr:
        curses.endwin()

def _display_element(x, x_offset=0):
    choice = _choices[x]
    params = [x+x_offset, 0, " "+choice+" "]

    if _highlight == STANDOUT:
        if x == _choice_index:
            params.append(curses.A_STANDOUT)
    elif _highlight == DIM_OTHERS:
        if x != _choice_index:
            params.append(curses.A_DIM)

    if _cursor:
        if x == _choice_index:
            params[2] = ">"+params[2]
        else:
            params[2] = " "+params[2]

    _stdscr.addstr(*params)

def _display_list():
    x_offset = 0
    if _title:
        _stdscr.addstr(0, 0, _title, curses.A_BOLD)
        x_offset = 1

    x = 0
    for choice in _choices:
        _display_element(x, x_offset)
        x+=1

    _stdscr.refresh()

def _keyboard_listen(q_disable=False):
    _stdscr.keypad(True)
    while True:
        c = _stdscr.getch()
        if c == curses.KEY_UP:
            _up()
        elif c == curses.KEY_DOWN:
            _down()
        elif c == curses.KEY_ENTER or c == 10 or c == 13:
            break
        elif c == ord('q') and not q_disable:
            _no_choice()
            break

def    _no_choice():
    global _choice_index
    _choice_index = -1

def    _up():
    global _choice_index
    if _choice_index > 0:
        _choice_index-=1
    else:
        _choice_index = len(_choices)-1
    _display_list()

def    _down():
    global _choice_index
    if _choice_index < len(_choices)-1:
        _choice_index+=1
    else:
        _choice_index=0
    _display_list()

def main(choices):
    title=''
    for x in range(len(choices)):
        ch = choices[x]
        if ch[0:2] == '-t':
            title = choices.pop(x)[2:]
            break

    create_list(choices)
    select(title)
    print(get_choice())

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])

