" VARIABLES --
    set tabstop=2                  "tab indent
    set shiftwidth=2               "'>' indent
    set expandtab                  "make tab out of spaces
    set softtabstop=2              "erase the whole tab with backspace
    set autoindent
    set foldmethod=syntax          "folding (was indent previously)
    set foldlevelstart=20
    set mouse=a                    "enable mouse
    "set clipboard=unnamedplus      "sync yank with clipboard (requires vim-gtk)
    "set number                     "enable line number
    set laststatus=2               "always add the statusbar
    set hlsearch                   "make sure search word is highlighted
    set incsearch                  "sets incremental search
    "set ignorecase                 "search ignores case
    set smartcase                  "search doesnt ignore case if capital letters
    set splitright                 "split will show up on the right
    set splitbelow                 "split will show up on the bottom
    set nowrap                     "dont wrap lines that are to long
    set undofile				   "make vim save undo history to files




    set undodir=~/.vim/undodir	   "set the directory for undofiles
    "set showtabline=2
    "set noshowmode                 "for lightline
    set hidden                     "can move between buffers without saving
    set wildmenu
    set wildmode=longest:full,full "options for menu that pops up when you press tab
    set cursorline                 "highlight line that cursor is on
    set showcmd                    "show my shortcuts in statusline

    "gvim cutsomizations
    set guioptions-=m  "remove menu bar
    set guioptions-=T  "remove toolbar
    set guioptions-=r  "remove right-hand scroll bar
    set guioptions-=L  "remove left-hand scroll bar

" COMMAND MAPPINGS --
    " make you save, like if you opened vim with sudo
    cnoremap w!! w !sudo tee > /dev/null %

    " quit and write-quit typos
    cnoreabbrev W w
    cnoreabbrev Q q
    cnoreabbrev Wq wq
    cnoreabbrev WQ wq
    cnoreabbrev qq qa
    cnoreabbrev Qq qa
    cnoreabbrev Tabe tabe
    cnoreabbrev Tabe tabe
    cnoreabbrev hwholeline let @/ = '.*\%(' . @/ . '\m\).*'

    " reload vimrc easier
    cnoreabbrev reload source ~/.vimrc

" AUTO COMMANDS
    autocmd BufWritePre * %s/\s\+$//e
    autocmd BufRead,BufNewFile *.sm set syntax=ruby

" KEY MAPPINGS --
    "set space to be leader
    let mapleader = "\<Space>"

    " locking arrow keys
    noremap <Left>  <NOP>
    noremap <Right> <NOP>
    noremap <Up>    <NOP>
    noremap <Down>  <NOP>

    " map ctrl+c to esc
    inoremap <C-c> <esc>
    vnoremap <C-c> <esc>

	"mapping enter to run recording on e
    "save quit shortcuts
    nnoremap <leader>w :w<cr>
    nnoremap <leader>q :q<cr>

    "make comma repeat recording
    nnoremap , @e

    "map d to delete
    nnoremap <leader>d "_d
    vnoremap <leader>d "_d

    "paste in place of visual selection
    vnoremap p "_dp
    vnoremap P "_dP

    "navigate paragraphs with arrow keys
    noremap <C-Up> {
    noremap <C-Down> }
    noremap ø {
    noremap æ }
    noremap <C-K> {
    noremap <C-J> }

    "remap split to ctrl-W ctrl-H
    "noremap <C-W><C-H> :split<cr>
    "noremap <C-W>h     :split<cr>
    "noremap <C-W><C-V> :vsplit<cr>
    "noremap <C-W>v     :vsplit<cr>

    ""map ctrl+t to new tab
    "noremap <C-W><C-T> :tabe .<cr>
    "noremap <C-W>t     :tabe .<cr>

    ""create session file
    "noremap <leader>e  :mksession! .session.vim<cr>
    "noremap <leader>r  :source .session.vim<cr>

    " map window switching shortcuts
    noremap <leader>H <C-W><left>
    noremap <leader>L <C-W><right>
    noremap <leader>K <C-W><up>
    noremap <leader>J <C-W><down>

    "map switch buffer
    nnoremap <C-h> :bp<cr>
    nnoremap <C-l> :bn<cr>
    nnoremap <leader>e :e<leader>
    nnoremap <leader>c :bdelete<cr>
    nnoremap <leader>b :ls<cr>:b

    "map switch tab
    noremap <leader>k :tabprevious<cr>
    noremap <leader>j :tabnext<cr>

    "map search properly
    noremap  * mkHml`k*`lzt`k
    vnoremap * "ny:set hlsearch<cr>:let @/ = @n<cr>

    ""map ctrl-c to flush clipboard register to clipboard using xsel
    "vnoremap <C-c> :w ! xsel -ib<home>silent <end><cr>

    "replace a selection with output of terminal if selection was to run as shell script
    vnoremap <C-r> :w! /tmp/vim_infile_bash<cr>'>:r! sh /tmp/vim_infile_bash<cr>'<V'>"_d

    "type shell commands quicker with
    noremap ! :!

    "substitute entire document
    nnoremap <leader>s <esc>:%s///g<left><left>
    vnoremap <leader>s :s///g<left><left>

    "shortcut for opening filebrowser in window
    noremap <leader>f :find %:p:h<cr>

    "shortcut for opening filebrowser in new tab
    nnoremap T :tabe %:p:h<cr>

    "autoindent whole document
    noremap <leader>= mnggVG='n

    "shortcut for no higlight :noh
    noremap <leader>* :noh<cr>

    "duplicate line
    noremap <leader>p VyP

    "open current file in new tab
    nnoremap <leader>t mn:tabe %<cr>'n

    "run selection in vimscript
    vnoremap <leader>r "py:@p<cr>

    "search with ignore case
    nnoremap \ /\c

" VUNDLE CONFIGURATIONS --
    set nocompatible              " be iMproved, required
    filetype off                  " required

    " set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()
    " alternatively, pass a path where Vundle should install plugins
    "call vundle#begin('~/some/path/here')

    " let Vundle manage Vundle, required
    Plugin 'VundleVim/Vundle.vim'

    " custom plugins
    Plugin 'itchyny/lightline.vim'
    Plugin 'wolf-dog/lightline-sceaduhelm.vim'
    Plugin 'airblade/vim-gitgutter'
    Plugin 'tpope/vim-fugitive'
    Plugin 'mbbill/undotree'
    Plugin 'godlygeek/tabular'
    Plugin 'bartlomiejdanek/vim-dart'
    Plugin 'kien/ctrlp.vim'
    Plugin 'nanotech/jellybeans.vim'
    Plugin 'udalov/kotlin-vim'
    Plugin 'google/vim-jsonnet'

    " my plugin
    "Plugin 'https://gitlab.com/foratseif/4vim'
    Plugin 'git@gitlab.com:foratseif/4vim.git'


    " All of your Plugins must be added before the following line
    call vundle#end()            " required
    filetype plugin indent on    " required

    " To ignore plugin indent changes, instead use:
    "filetype plugin on

" PLUGIN CONFIURATIONS --
    " # netrw
    let g:netrw_liststyle = 3

    " # CtrlP
    set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
    let g:ctrlp_custom_ignore = '\v[\/](\.(git|hg|svn))|(node_modules)$'
    let g:ctrlp_working_path_mode = 'ra'

    " # lightline
    let g:lightline = {
      \ 'colorscheme': 'sceaduhelm',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }
    " coloschemes:
    "  - sceaduhelm
    "  - powerline

    " close NERDTree if its the last open window
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

    " # undotree
    noremap <leader>u :UndotreeToggle\|UndotreeFocus<CR>

    " # Tabular
    vnoremap <leader>t :Tabularize /


" THEMES AND COLORS
    syntax enable
    set background=dark

    let g:jellybeans_overrides = {
    \   'background': { 'ctermbg': 'none', '256ctermbg': 'none' },
    \   'VertSplit':  { 'ctermbg': 'none', 'ctermfg': 'none' },
    \}

    colorscheme jellybeans

    "hi VertSplit   ctermbg=242   ctermfg=0
    hi TabLine     ctermfg=Black ctermbg=Green      cterm=NONE
    hi TabLineFill ctermfg=Black ctermbg=Green      cterm=NONE
    hi TabLineSel  ctermfg=White ctermbg=DarkBlue   cterm=NONE
    hi TabLineSel  ctermfg=White ctermbg=DarkBlue   cterm=NONE
    hi Search      ctermfg=Black ctermbg=Yellow     cterm=NONE
    hi Visual      ctermfg=Black ctermbg=DarkYellow cterm=NONE
    hi MatchParen  ctermfg=Red ctermbg=none cterm=bold

    " turn vue files to html
    autocmd BufRead,BufNewFile *.vue setfiletype html

    " FAVORITE COLORSCHEMES :
        " monokai
        " buddy
        " evening
        " triplejelly
        " gruvbox

" CUSTOM FUNCTIONS

    function! DoWrap()
        setlocal wrap linebreak nolist linebreak textwidth=0 wrapmargin=0
        set virtualedit=
        setlocal display+=lastline
        noremap  <buffer> <silent> k gk
        noremap  <buffer> <silent> j gj
    endfunction

    function! UndoWrap()
        setlocal nowrap textwidth=70 wrapmargin=0
        set virtualedit=all
        silent! nunmap <buffer> k
        silent! nunmap <buffer> j
    endfunction

    function! DoFlutter()
        setlocal tabstop=2 shiftwidth=2 softtabstop=2
    endfunction

    function! UndoFlutter()
        "set tabstop=4 shiftwidth=4 softtabstop=4
    endfunction

" FILE SPESIFIC CONFIGS
    au BufEnter *.txt,*.tex,*.md call DoWrap()
    au BufLeave *.txt,*.tex,*.md call UndoWrap()

    au BufEnter *.dart, call DoFlutter()
    au BufLeave *.dart, call UndoFlutter()

    autocmd FileType jsonnet setlocal shiftwidth=2 tabstop=2 expandtab softtabstop=2
