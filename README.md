# Description
Keeping track of my linux dot configs files.

# Clone
Remember to clone with ```--recursive``` flag
```
  git clone --recursive https://github.com/foratseif/dotfiles
```

# Install
Navigate inside the ```dotfiles``` directory and run
```
	./install.sh
```
This script places the dotfiles in the correct paths.
It can also use some flags:
- ```-y``` yes for all. The script will create .f_backup files if it finds old config files about to be overwritten.
- ```-o``` overrides without backing up.
- ```--clean``` removes all the .f_backup files created by the script.
